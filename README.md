# MOTD-Screenfetch

A script that automatically install itself and generate a dynamic MOTD for SSH logins 

**NOTE:** This code isn't entirely mine, some functions are copied from the screenfetch project and others are from random websites, if you are the copyright owner of some of the code please see my address at the copyright part of the code

**NOTE:** This script is primarly intended for Debian users

**NOTE:** For automatic install you must have PAM and the folder /etc/update-motd.d present, otherwise... find your way of making it work

## Installation

Clone GitHub Repository

     $ git clone https://gitlab.com/TanyaEleventhGoddess/motd-screenfetch && cd motd-screenfetch

### Debian and derivates

#### Installing

Install as package

     $ dpkg-buildpackage -us -uc
     $ sudo apt-get install ../motd-screenfetch*.deb

Install with sudo

     $ sudo ./motd-screenfetch

Install without sudo

     # cp ./motd-screenfetch /etc/update-motd.d/15-motd-screenfetch
     # chmod 644 /etc/update-motd.d/10-uname

#### Setting pam to ignore the static MOTD

SSH:

     # nano /etc/pam.d/sshd
     "session    optional     pam_motd.so noupdate" > "#session    optional     pam_motd.so noupdate"
     
tty:

     # nano /etc/pam.d/login
     "session    optional     pam_motd.so noupdate" > "#session    optional     pam_motd.so noupdate"

#### Installing optional dependencies

     # apt-get install dmidecode
     
#### Raspbian only:

     # mkdir /usr/share/motd-screenfetch/ && cp ./enhancers/raspi_expander /usr/share/motd-screenfetch/expander

## Notifications system

Scripts and other applications can display advices with motd-screenfetch by placing them in the default /tmp/motd-screenfetch/notices/$USER folder or any specified folder by placing files in this way:

     name of the program
       |
       |--file with the Notifications
       
 Sourcing /etc/default/motd-screenfetch for knowing the notification directory is recommended

## Expansion

The bash script can be expanded by placing more code that output his results to OPTIONAL[@] array

The script is configured to use /usr/share/motd-screenfetch/expand
